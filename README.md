# Atividade-Backend-Sistema-Z
Um Sistema Z enviará para nós uma requisição POST com o status do sistema no formato JSON a cada alteração interna. Precisamos armazenar esta <br> informação em um banco de dados SQL Server em duas tabelas por meio de Store Procedure. <br>
<br>
Tipo do Projeto: ASP .NET CORE Web API <br>
Ferramentas utilizadas:<br>
1 - Visual Studio 2019;<br>
2 - Packages Manager NuGet;<br>
2.1 - Newtonsoft.Json v13.0.1;<br>
2.2 - Swashbuckle.AspNetCore v5.6.3;<br>
2.3 - System.Data.SqlClient v4.8.2;<br>
3 - Target Framework: .NET 5.0;<br>
4 - Microsoft SQL Server Management Studio 2016 (Usuário local); <br>
4.1 - Server type: Database Engine;<br>
4.1 - Usuário: (LocalDB)\MSSQLLocalDB;<br>
4.2 - Authentication: Windows Authentication;<br>
4.3 - Database: System Database/master<br>
4.4 - "connectionStrings": {defaultConnection": "Data Source=(LocalDB)\MSSQLLocalDB; Initial Catalog=master;"<br>
4.5 - Tabelas: dbo.log,dbo.controle <br>
4.6 - Procedures: dbo.usp_GravarRegistro<br>

# Tabelas


```sql
USE [master]
GO
CREATE TABLE [dbo].[controle](
	[id] [varchar](50) NULL,
	[status] [varchar](255) NULL,
	[date] [datetime] NULL
) ON [PRIMARY]

GO



USE [master]
GO
CREATE TABLE [dbo].[log](
	[idSeq] [int] IDENTITY(1,1) NOT NULL,
	[dtRegistro] [datetime] NOT NULL,
	[jsonLog] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idSeq] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

```


# Procedures
Nome: usp_ListaRegistro

```sql

USE [master]
CREATE procedure [dbo].[usp_ListaRegistro] 
as 
begin 
select l.idSeq, convert(datetime,l.dtRegistro) as dtRegistro, l.jsonLog from log  l
end

```


Nome: [usp_GravarRegistro]

```sql
USE master
alter procedure dbo.usp_GravarRegistro
@jsonLog varchar(max) = null,
@status varchar(max) = null
as
begin
	update controle set status = @status where id = 'Sistema Z'
	insert into log values (getdate(),@jsonLog)
	select * from log
end

```


ValuesController.cs: Responsável pela gravação


Endpoints
POST: /api/Values

Schemas:
```json
{
date: string nullable: true
status: string nullable: true
}
```

REQUEST
EXEMPLO: POST

```json
{
	"date": "2021-05-18 10:00:00",
	"status": "Sistema  em funcionamento"
}
```


RESPONSE: 200OK

EXEMPLO: GET

```json
[
    {
        "idSeq": 3002,
        "dtRegistro": "16/06/2021 16:42:58",
        "jsonLog": "{\"date\":\"2021-05-18 10:00:00\",\"status\":\"Sistema em funcionamento\"}"
    },
    {
        "idSeq": 3003,
        "dtRegistro": "16/06/2021 16:44:54",
        "jsonLog": "{\"date\":\"2021-05-18 10:00:00\",\"status\":\"Sistema em funcionamento\"}"
    },
    {
        "idSeq": 3004,
        "dtRegistro": "16/06/2021 16:56:56",
        "jsonLog": "{\"date\":\"2021-05-18 10:00:00\",\"status\":\"Sistema em funcionamento\"}"
    },
    {
        "idSeq": 3005,
        "dtRegistro": "16/06/2021 16:56:56",
        "jsonLog": "{\"date\":\"2021-05-18 10:00:00\",\"status\":\"Sistema em funcionamento\"}"
    }
]

```





# Avaliação

### Problema

Um Sistema Z enviará para nós uma requisição POST com o status do sistema no formato JSON a cada alteração interna. Precisamos armazenar esta informação em um banco de dados SQL Server em duas tabelas por meio de Store Procedure.

### Desafio

O candidato precisa criar:

1. uma solução em .NET 5+ (ou .NetCore 3.1) para receber a requisição do Sistema Z e enviar para a Store Procedure no SQL Server
2. criar a Store Procedure que fará duas tarefas:
    1. atualiza o status do Sistema Z na tabela "controle"
    2. insere o JSON recebido na tabela "log"

Pontos de avaliação do candidato:

* Registro por meio de git commit das etapas de produção dos códigos
* Organização dos códigos e documentação
* Manual de como preparar para executar o código

### Dados iniciais

O sistema Z enviará um POST para uma URL (para testes locais algo como http://127.0.0.1/api) com o seguinte formato JSON:

```
{
	'date': '2021-05-18 10:00:00',
	'status': 'Sistema em funcionamento'
}
```

Outro exemplo

```
{
	'date': '2021-05-18 10:00:00',
	'status': 'Sistema parado',
	'cause': 'Reach max users requests'
}
```

Dica: simule o envio do JSON usando o programa Postman ou similar. A resposta da requisição deve ser um HTTP 200 OK quando sucesso e HTTP 400 caso o JSON esteja fora do padrão. 

Para o SQL Server inicie um database com o seguinte código:

```
CREATE TABLE controle (
    id varchar(50),
    status varchar(255),
    date datetime
);
INSERT INTO controle (id, status, date) VALUES ('Sistema Z', '', '2021-05-18 00:00:00');
```

### Entrega

Primeiramente, você deve fazer um fork deste repositório, e versionar o seu código nele; Para isto é necessário ter cadastro no www.gitlab.com.

Forneça as instruções em um arquivo INSTRUCOES.md sobre como montar a sua aplicação, mencionando cada etapa necessária para que consigamos colocá-la para rodar. Especifique qual banco de dados você usou, e qual versão dos recursos utilizados.

Quando terminar, você deve entregar o código fazendo um merge request. Atenção: uma vez submetido o request, o seu teste estará finalizado e não será mais possível voltar atrás.

Boa sorte!
